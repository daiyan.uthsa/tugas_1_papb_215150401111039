import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Tugas1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Color(0xFF1171BE),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 340, left: 88, right: 88),
            child: Text(
              'Daiyan Uthsa Rafif',
              style: GoogleFonts.poppins(
                  color: Color(0xFFF1F1F1),
                  fontSize: 24,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 380, left: 108, right: 108),
            child: Text(
              '215150401111039',
              style: GoogleFonts.poppins(
                  color: Color(0xFFF1F1F1),
                  fontSize: 24,
                  fontWeight: FontWeight.w500),
            ),
          )
        ],
      ),
    );
  }
}
